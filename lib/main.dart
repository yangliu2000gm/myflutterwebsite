// import 'package:flutter/material.dart';

// void main() => runApp(SignUpApp());

// class SignUpApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       routes: {
//         '/': (context) => SignUpScreen(),
//       },
//     );
//   }
// }

// class SignUpScreen extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.grey[200],
//       body: Center(
//         child: SizedBox(
//           width: 400,
//           child: Card(
//             child: SignUpForm(),
//           ),
//         ),
//       ),
//     );
//   }
// }

// class SignUpForm extends StatefulWidget {
//   @override
//   _SignUpFormState createState() => _SignUpFormState();
// }

// class _SignUpFormState extends State<SignUpForm> {
//   final _firstNameTextController = TextEditingController();
//   final _lastNameTextController = TextEditingController();
//   final _usernameTextController = TextEditingController();

//   double _formProgress = 0;

//   @override
//   Widget build(BuildContext context) {
//     return Form(
//       child: Column(
//         mainAxisSize: MainAxisSize.min,
//         children: [
//           LinearProgressIndicator(value: _formProgress),
//           Text('Sign up', style: Theme
//               .of(context)
//               .textTheme
//               .headline4),
//           Padding(
//             padding: EdgeInsets.all(8.0),
//             child: TextFormField(
//               controller: _firstNameTextController,
//               decoration: InputDecoration(hintText: 'First name'),
//             ),
//           ),
//           Padding(
//             padding: EdgeInsets.all(8.0),
//             child: TextFormField(
//               controller: _lastNameTextController,
//               decoration: InputDecoration(hintText: 'Last name'),
//             ),
//           ),
//           Padding(
//             padding: EdgeInsets.all(8.0),
//             child: TextFormField(
//               controller: _usernameTextController,
//               decoration: InputDecoration(hintText: 'Username'),
//             ),
//           ),
//           TextButton(
//             style: ButtonStyle(
//               foregroundColor: MaterialStateColor.resolveWith((Set<MaterialState> states) {
//                 return states.contains(MaterialState.disabled) ? null : Colors.white;
//               }),
//               backgroundColor: MaterialStateColor.resolveWith((Set<MaterialState> states) {
//                 return states.contains(MaterialState.disabled) ? null : Colors.blue;
//               }),
//             ),
//             onPressed: null,
//             child: Text('Sign up'),
//           ),
//         ],
//       ),
//     );
//   }
// }
// 
// 
// 





import 'package:flutter/material.dart';
import 'package:newwebpage/screens/home/home_screen.dart';
import 'package:newwebpage/screens/login/login.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Map<String, WidgetBuilder> routes = {
      // '/': (BuildContext context) => Login(),
      // '/home': (BuildContext context) => HomeScreen(),
      '/': (BuildContext context) => HomeScreen(),
      '/home': (BuildContext context) => HomeScreen(),
    };
    return MaterialApp(
      initialRoute: '/',
      routes: routes,
      theme: ThemeData(fontFamily: 'HelveticaNeue'),
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
    );
  }
}

