import 'package:flutter/material.dart';

class Getstart extends StatefulWidget {

  @override
  GetstartState createState() {
    return GetstartState();
  }
}

class GetstartState extends State<Getstart> {


  @override
  void initState() {

    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return SingleChildScrollView( 
    child:Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(child:
                        Container(
                          color: Colors.blue,
                          padding: EdgeInsets.only(top: 8, bottom: 8),
                          child: Text("Try the SMS API"),
                        ),
              )
            ],
          ),

          Text("Movider makes sending SMS easy. Try our API by sending an SMS to your phone. Sending SMS uses your Movider balance."),

          SizedBox(height:30),
          Text("Try it out"),

          Divider(
            color: Colors.black,
          ),


 
              Flex(
                direction: Axis.horizontal,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Container(
                      height: 500.0,
                      // color: Colors.red,
                      child:Column(
                      children: <Widget>[

                        TextFormField(
                          initialValue: 'Input text',
                          decoration: InputDecoration(
                            labelText: 'Label text',
                            // errorText: 'Error message',
                            border: OutlineInputBorder(),
                            // suffixIcon: Icon(
                            //   Icons.error,
                            // ),
                          ),
                        ),

                        SizedBox(height:10),

                        Text("This example only sends a message to the number you registered."),

                        TextFormField(
                          maxLines: 10,
                          initialValue: '''Hello!
                          Thank you for doing a message trial with Movider.
                          Have a great day ahead.''',
                          decoration: InputDecoration(
                            labelText: 'Label text',
                            // errorText: 'Error message',
                            border: OutlineInputBorder(),
                            // suffixIcon: Icon(
                            //   Icons.error,
                            // ),
                          ),
                        ), 


                      ],
                      ),

                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Container(
                      height: 30.0,
                      // color: Colors.green,
                    ),
                  ),
                ],
              ),



        ],
    ),
    );

  }
}