import 'package:flutter/material.dart';
import 'package:newwebpage/screens/dashboard/dashboard.dart';
import 'package:newwebpage/screens/forms/form.dart';
import 'package:newwebpage/screens/hero/hero_screen.dart';
import 'package:newwebpage/utils/color_constants.dart';
import 'package:newwebpage/screens/sendsms/sendsms.dart';
import 'package:newwebpage/screens/report/report.dart';
import 'package:newwebpage/screens/get_starting/getstarting.dart';
class HomeScreen extends StatefulWidget {
  @override
  HomeScreenState createState() => HomeScreenState();
}

class NewItem {
  bool isExpanded;
  final String header;
  final Widget body;
  final Icon iconpic;
  NewItem(this.isExpanded, this.header, this.body, this.iconpic);
}


class HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  TabController tabController;
  int active = 0;
  List<NewItem> items;




  @override
  void initState() {
    super.initState();
    tabController = new TabController(vsync: this, length: 4, initialIndex: 0)
      ..addListener(() {
        setState(() {
          active = tabController.index;
        });
      });

    items = <NewItem>[
    new NewItem(
        false,
        'BulkSms',
        new Padding(
            padding: new EdgeInsets.all(20.0),
            child: new Column(
                children: <Widget>[
                    //put the children here
                      FlatButton(
                        color: Colors.grey[100],
                        onPressed: () {
                          tabController.animateTo(0);
                        },

                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Container(
                            padding: EdgeInsets.only(top: 22, bottom: 22, right: 22),
                            child: Row(children: [
                              Icon(Icons.dashboard),
                              SizedBox(
                                width: 8,
                              ),
                              Text(
                                "Send SMS",
                                style: TextStyle(
                                  fontSize: 18,
                                  fontFamily: 'HelveticaNeue',
                                ),
                              ),
                            ]),
                          ),
                        ),
                      ),

                      FlatButton(
                        color: Colors.grey[100],
                        onPressed: () {
                          tabController.animateTo(1);

                        },

                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Container(
                            padding: EdgeInsets.only(top: 22, bottom: 22, right: 22),
                            child: Row(children: [
                              Icon(Icons.dashboard),
                              SizedBox(
                                width: 8,
                              ),
                              Text(
                                "Report",
                                style: TextStyle(
                                  fontSize: 18,
                                  fontFamily: 'HelveticaNeue',
                                ),
                              ),
                            ]),
                          ),
                        ),
                      ),                      
                ])
              ),new Icon(Icons.cloud)),
     new NewItem(
        false,
        'SMS',
        new Padding(
            padding: new EdgeInsets.all(20.0),
            child: new Column(
                children: <Widget>[
                    //put the children here
                      FlatButton(
                        color: Colors.grey[100],
                        onPressed: () {
                          tabController.animateTo(3);
                        },

                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Container(
                            padding: EdgeInsets.only(top: 22, bottom: 22, right: 22),
                            child: Row(children: [
                              Icon(Icons.dashboard),
                              SizedBox(
                                width: 8,
                              ),
                              Text(
                                "Getting started",
                                style: TextStyle(
                                  fontSize: 18,
                                  fontFamily: 'HelveticaNeue',
                                ),
                              ),
                            ]),
                          ),
                        ),
                      ),
                      //
                      FlatButton(
                        color: Colors.grey[100],
                        onPressed: () {
                          tabController.animateTo(0);
                        },

                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Container(
                            padding: EdgeInsets.only(top: 22, bottom: 22, right: 22),
                            child: Row(children: [
                              Icon(Icons.dashboard),
                              SizedBox(
                                width: 8,
                              ),
                              Text(
                                "Overview",
                                style: TextStyle(
                                  fontSize: 18,
                                  fontFamily: 'HelveticaNeue',
                                ),
                              ),
                            ]),
                          ),
                        ),
                      ),
                                            FlatButton(
                        color: Colors.grey[100],
                        onPressed: () {
                          tabController.animateTo(0);
                        },

                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Container(
                            padding: EdgeInsets.only(top: 22, bottom: 22, right: 22),
                            child: Row(children: [
                              Icon(Icons.dashboard),
                              SizedBox(
                                width: 8,
                              ),
                              Text(
                                "Delivery by day ",
                                style: TextStyle(
                                  fontSize: 18,
                                  fontFamily: 'HelveticaNeue',
                                ),
                              ),
                            ]),
                          ),
                        ),
                      ),

                      FlatButton(
                        color: Colors.grey[100],
                        onPressed: () {
                          tabController.animateTo(0);
                        },

                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Container(
                            padding: EdgeInsets.only(top: 22, bottom: 22, right: 22),
                            child: Row(children: [
                              Icon(Icons.dashboard),
                              SizedBox(
                                width: 8,
                              ),
                              Text(
                                "Delivery by month",
                                style: TextStyle(
                                  fontSize: 18,
                                  fontFamily: 'HelveticaNeue',
                                ),
                              ),
                            ]),
                          ),
                        ),
                      ),

                                            FlatButton(
                        color: Colors.grey[100],
                        onPressed: () {
                          tabController.animateTo(0);
                        },

                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Container(
                            padding: EdgeInsets.only(top: 22, bottom: 22, right: 22),
                            child: Row(children: [
                              Icon(Icons.dashboard),
                              SizedBox(
                                width: 8,
                              ),
                              Text(
                                "Search message",
                                style: TextStyle(
                                  fontSize: 18,
                                  fontFamily: 'HelveticaNeue',
                                ),
                              ),
                            ]),
                          ),
                        ),
                      ),

                ])
              ),new Icon(Icons.cloud)),
              //give all your items here
  ];
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading:
            MediaQuery.of(context).size.width < 1300 ? true : false,
        title: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 32,top: 5,bottom: 5),
                // child: Text(
                //   "Movider",
                //   style: TextStyle(
                //     fontSize: 24,
                //     color: Colors.white,
                //     fontWeight: FontWeight.bold,
                //     fontFamily: 'HelveticaNeue',
                //   ),
                // ),
                child: Image.asset('logo.png'),
              ),
            ]),
        actions: <Widget>[
            PopupMenuButton<String>(
            // onSelected: handleClick,
            child: Row(
              children: <Widget>[
                IconTheme(
                    data: new IconThemeData(
                        color: Colors.black), 
                    child: new Icon(Icons.people),
                ),
                Text('ryan.chwa@worldhubcom.com',style: TextStyle(color: Colors.black,decoration: TextDecoration.underline),),
              ],
            ),
            itemBuilder: (BuildContext context) {
              return {'Logout', 'Settings'}.map((String choice) {
                return PopupMenuItem<String>(
                  value: choice,
                  child: Text(choice),
                );
              }).toList();
            },
          ),

          // InkWell(
          //   onTap: () {
          //     print("download");
          //   },
          //   child: Container(
          //     margin: EdgeInsets.all(12),
          //     padding: EdgeInsets.all(8),
          //     decoration: BoxDecoration(
          //       borderRadius: BorderRadius.circular(5),
          //       color: Colors.white,
          //     ),
          //     child: Row(
          //       crossAxisAlignment: CrossAxisAlignment.end,
          //       children: <Widget>[
          //         Icon(
          //           Icons.download_done_rounded,
          //           color: Colors.black,
          //           size: 22,
          //         ),
          //         SizedBox(
          //           width: 4,
          //         ),
          //         Text(
          //           "Download Now",
          //           style: TextStyle(
          //             fontSize: 12,
          //             color: Colors.black,
          //             fontFamily: 'HelveticaNeue',
          //           ),
          //         ),
          //       ],
          //     ),
          //   ),
          // ),
        ],
        backgroundColor: Colors.white,
        // automaticallyImplyLeading: false,
      ),
      body: Row(
        children: <Widget>[
          MediaQuery.of(context).size.width < 1300
              ? Container()
              : Card(
                  elevation: 2.0,
                  child: Container(
                      margin: EdgeInsets.all(0),
                      height: MediaQuery.of(context).size.height,
                      width: 300,
                      color: Colors.white,
                      child: listDrawerItems(false)),
                ),
          Container(
            width: MediaQuery.of(context).size.width < 1300
                ? MediaQuery.of(context).size.width
                : MediaQuery.of(context).size.width - 310,
            child: TabBarView(
              physics: NeverScrollableScrollPhysics(),
              controller: tabController,
              children: [
                // Dashboard(),
                SendSMS(),
                // FormMaterial(),
                SMSReport(),
                HeroAnimation(),
                Getstart(),
              ],
            ),
          )
        ],
      ),
      drawer: Padding(
          padding: EdgeInsets.only(top: 56),
          child: Drawer(child: listDrawerItems(true))),
    );
  }

  Widget listDrawerItems(bool drawerStatus) {
    return ListView(
      children: <Widget>[
        Container(
          width:150,
          height:80,
          color:Colors.black,
          child:Center(

            child: Text("BALANCE: 50.79267214",
                      style: TextStyle(color: Colors.white),
          ),
          ),
        ),

        new Padding(
          padding: new EdgeInsets.all(10.0),
          child: new ExpansionPanelList(
            expansionCallback: (int index, bool isExpanded) {
              setState(() {
                items[index].isExpanded = !items[index].isExpanded;
              });
            },
            children: items.map((NewItem item) {
              return new ExpansionPanel(
                headerBuilder: (BuildContext context, bool isExpanded) {
                  return new ListTile(
                      leading: item.iconpic,
                      title: new Text(
                        item.header,
                        textAlign: TextAlign.left,
                        style: new TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.w400,
                        ),
                      ));
                },
                isExpanded: item.isExpanded,
                body: item.body,
              );
            }).toList(),
          ),
        )

      ],
    );
  }
}









    // List_Criteria = new ListView(
    //   children: [
    //     new Padding(
    //       padding: new EdgeInsets.all(10.0),
    //       child: new ExpansionPanelList(
    //         expansionCallback: (int index, bool isExpanded) {
    //           setState(() {
    //             items[index].isExpanded = !items[index].isExpanded;
    //           });
    //         },
    //         children: items.map((NewItem item) {
    //           return new ExpansionPanel(
    //             headerBuilder: (BuildContext context, bool isExpanded) {
    //               return new ListTile(
    //                   leading: item.iconpic,
    //                   title: new Text(
    //                     item.header,
    //                     textAlign: TextAlign.left,
    //                     style: new TextStyle(
    //                       fontSize: 20.0,
    //                       fontWeight: FontWeight.w400,
    //                     ),
    //                   ));
    //             },
    //             isExpanded: item.isExpanded,
    //             body: item.body,
    //           );
    //         }).toList(),
    //       ),
    //     )
    //   ],
    // );
