import 'package:flutter/material.dart';

class SendSMS extends StatefulWidget {

  @override
  SendSMSState createState() {
    return SendSMSState();
  }
}

class SendSMSState extends State<SendSMS> {

  List _cities =
  ["Cluj-Napoca", "Bucuresti", "Timisoara", "Brasov", "Constanta"];

  List<DropdownMenuItem<String>> _dropDownMenuItems;

  String _currentAPIKey;

  @override
  void initState() {
    _dropDownMenuItems = getDropDownMenuItems();
    _currentAPIKey = _dropDownMenuItems[0].value;
    super.initState();
  }


  // here we are creating the list needed for the DropDownButton
  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String city in _cities) {
      // here we are creating the drop down menu items, you can customize the item right here
      // but I'll just use a simple text for this
      items.add(new DropdownMenuItem(
          value: city,
          child: new Text(city)
      ));
    }
    return items;
  }

  void changedDropDownItem(String selectedCity) {
    print("Selected city $selectedCity, we are going to refresh the UI");
    setState(() {
      _currentAPIKey = selectedCity;
    });
  }
  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return SingleChildScrollView( 
    child:Form(
      // key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(child:
                        Container(
                          color: Colors.blue,
                          padding: EdgeInsets.only(top: 8, bottom: 8),
                          child: Text("Sending message"),
                        ),
              )
            ],

          ),

          SizedBox(height: 50),
          
          Container(
            width: 400,
            decoration: ShapeDecoration(
              shape: RoundedRectangleBorder(
                side: BorderSide(width: 1.0, style: BorderStyle.solid),
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
              ),
            ),

            child: new DropdownButtonHideUnderline(
            
            child:DropdownButton(
              value: _currentAPIKey,
              items: _dropDownMenuItems,
              onChanged: changedDropDownItem,
            ),
            ),
          ),

          SizedBox(height: 50),
          Container(
            width: 400,
            decoration: ShapeDecoration(
              shape: RoundedRectangleBorder(
                side: BorderSide(width: 1.0, style: BorderStyle.solid),
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
              ),
            ),

            child: new DropdownButtonHideUnderline(
            
            child:DropdownButton(
              value: _currentAPIKey,
              items: _dropDownMenuItems,
              onChanged: changedDropDownItem,
            ),
            ),
          ),


          SizedBox(height: 50),
          Container(
            width: 400,
            height: 200,
            decoration: BoxDecoration(
              // color: Colors.grey,

              border: Border.all(color: Colors.blueAccent),

              borderRadius:  BorderRadius.circular(32),
            ),
            child: TextField(
              decoration: InputDecoration(
                hintStyle: TextStyle(fontSize: 17),
                hintText: 'Search your trips',
                // suffixIcon: Icon(Icons.search),
                border: InputBorder.none,
                contentPadding: EdgeInsets.all(20),
              ),
            ),
          ),


          SizedBox(height: 50),
          Container(
            width: 400,
            height: 200,
            decoration: BoxDecoration(
              // color: Colors.grey,

              border: Border.all(color: Colors.blueAccent),

              borderRadius:  BorderRadius.circular(32),
            ),
            child: TextField(
              decoration: InputDecoration(
                hintStyle: TextStyle(fontSize: 17),
                hintText: 'Search your trips',
                // suffixIcon: Icon(Icons.search),
                border: InputBorder.none,
                contentPadding: EdgeInsets.all(20),
              ),
            ),
          ),

          SizedBox(height: 50),
          Container(
            width: 400,
            height: 100,
            decoration: BoxDecoration(
              // color: Colors.grey,

              border: Border.all(color: Colors.blueAccent),

              borderRadius:  BorderRadius.circular(32),
            ),
            child: TextField(
              decoration: InputDecoration(
                hintStyle: TextStyle(fontSize: 17),
                hintText: 'Company name',
                // suffixIcon: Icon(Icons.search),
                border: InputBorder.none,
                contentPadding: EdgeInsets.all(20),
              ),
            ),
          ),

          SizedBox(height:50),

          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(width:300),
              RaisedButton(
                onPressed: () {},
                color: Colors.blue,
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0),
                ),
                child: Text(
                  'Send SMS',
                  style: TextStyle(fontSize: 20),
                ),
              ),
    
            ],

          ),




        ],
      ),
    ),
    );
  }
}