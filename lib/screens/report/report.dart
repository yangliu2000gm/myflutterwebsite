import 'package:flutter/material.dart';

// Create a Form widget.
class SMSReport extends StatefulWidget {

  @override
  SMSReportState createState() {
    return SMSReportState();
  }
}

class SMSReportState extends State<SMSReport> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(child:
                        Container(
                          color: Colors.blue,
                          padding: EdgeInsets.only(top: 8, bottom: 8),
                          child: Text("Report"),
                        ),
              )
            ],
          ),


          SizedBox(
            height:100,
          ),
          
          Table(
            border: TableBorder.all(), // Allows to add a border decoration around your table
            children: [ 
              TableRow(children :[
                Text('Year'),
                Text('Lang'),
                Text('Author'),
              ]),
              TableRow(children :[
                Text('2011',),
                Text('Dart'),
                Text('Lars Bak'),
              ]),
              TableRow(children :[
                Text('1996'),
                Text('Java'),
                Text('James Gosling'),
              ]),
            ]
          ),

          
        ],
    );

  }
}